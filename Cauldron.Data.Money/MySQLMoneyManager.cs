/*
 * Copyright (c) Contributors, http://opensimulator.org/, http://www.nsl.tuis.ac.jp/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *	 * Redistributions of source code must retain the above copyright
 *	   notice, this list of conditions and the following disclaimer.
 *	 * Redistributions in binary form must reproduce the above copyright
 *	   notice, this list of conditions and the following disclaimer in the
 *	   documentation and/or other materials provided with the distribution.
 *	 * Neither the name of the OpenSim Project nor the
 *	   names of its contributors may be used to endorse or promote products
 *	   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using log4net;
using MySql.Data.MySqlClient;
using OpenMetaverse;
using Migration = OpenSim.Data.Migration;

namespace Cauldron.Data.Money
{
	public class MySQLMoneyManager:IMoneyManager
	{
		private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private string Table_of_Balance	 	= "balances";
		private string Table_of_UserInfo	= "userinfo";
		private string Table_of_Transaction = "transactions";

        private string m_connectionString;

		private int m_defaultMoney = 0;

        private MySqlConnection m_connection;
  
        protected virtual Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

		public MySQLMoneyManager(string hostname, string database, string username,
                                 string password, string cpooling, string port)
		{
            string s = String.Format("Server={0};port={1};database={2};User ID={3};Password{4};Pooling={5}", 
                                     hostname, port, database, username, password, cpooling);
			Initialise(s);
		}



		public MySQLMoneyManager(string connect)
		{
			Initialise(connect);
		}



		private void Initialise(string connect)
		{
			try
			{
				m_connectionString = connect;
                m_connection = new MySqlConnection(m_connectionString);

				try
				{
					m_connection.Open();
				}
				catch (Exception e)
				{
					throw new Exception("Connection error while using connection string " + m_connectionString, e);
				}

                Migration m = new Migration(m_connection, Assembly, "MoneyMySQL");
                m.Update();
            }
            catch (Exception e)
			{
				throw new Exception("Error initialising MySql Database: " + e.ToString());
			}


		}


		///////////////////////////////////////////////////////////////////////


		/// <summary>
		/// Reconnect to the database
		/// </summary>
		public void Reconnect()
		{
			m_log.Info("Reconnecting database...");
			lock (m_connection)
			{
				try
				{
					m_connection.Close();
					m_connection = new MySqlConnection(m_connectionString);
					m_connection.Open();
					m_log.Info("Reconnected database");
				}
				catch (Exception e)
				{
                    m_log.ErrorFormat("Unable to reconnect to database: {0}", e.ToString());
				}
			}
		}



		///////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Get balance from database. returns -1 if failed.
		/// </summary>
		/// <param name="userID"></param>
		/// <returns></returns>
		public int getBalance(string userID)
		{
			string sql = string.Empty;
			int retValue = -1;
			sql += "SELECT balance FROM " + Table_of_Balance + " WHERE user = ?userid";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?userid", userID);
			try
			{
				using (MySqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.SingleRow))
				{
					if (dbReader.Read())
					{
						retValue = Convert.ToInt32(dbReader["balance"]);
					}
					dbReader.Close();
					cmd.Dispose();
				}
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("MySql failed to fetch balance {0}{1}{2}{3} {4}",
                                  Environment.NewLine, e.ToString(), Environment.NewLine, "Reconnecting", userID);
				Reconnect();
				return -2;
			}
			return retValue;
		}


		public bool updateBalance(string uuid, int amount)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "UPDATE " + Table_of_Balance + " SET balance = ?amount where user = ?uuid;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?amount", amount);
			cmd.Parameters.AddWithValue("?uuid", uuid);

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Update money error: {0}", e.ToString());
				return false;
			}

			return bRet;
		}


		public bool addUser(string userID,int balance,int status)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "INSERT INTO " + Table_of_Balance + " (`user`,`balance`,`status`) VALUES ";
			sql += "(?userID,?balance,?status);";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);

			cmd.Parameters.AddWithValue("?userID",  userID);
			cmd.Parameters.AddWithValue("?balance", balance);
			cmd.Parameters.AddWithValue("?status",  status);
			if (cmd.ExecuteNonQuery() > 0) bRet = true;

			return bRet;
		}


		/// <summary>
		/// Add user,for internal use
		/// </summary>
		/// <param name="userID"></param>
		private void addUser(string userID)
		{
			string sql = string.Empty;

			sql += "INSERT INTO " + Table_of_Balance + " (`user`,`balance`,`status`) VALUES ";
			sql += "(?userID,?balance,?status)";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);

			cmd.Parameters.AddWithValue("?userID",  userID);
			cmd.Parameters.AddWithValue("?balance", m_defaultMoney);
			cmd.Parameters.AddWithValue("?status",  0);
			cmd.ExecuteNonQuery();
		}


		/// <summary>
		/// Here we'll make a withdraw from the sender and update transaction status
		/// </summary>
		/// <param name="fromID"></param>
		/// <param name="toID"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public bool withdrawMoney(UUID transactionID,string senderID, int amount)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "BEGIN;";
			sql += "UPDATE " + Table_of_Balance + " SET balance = balance - ?amount where user = ?userid;";
			sql += "UPDATE " + Table_of_Transaction + " SET status = ?status where UUID  = ?tranid;";
			sql += "COMMIT;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);

			cmd.Parameters.AddWithValue("?amount", amount);
			cmd.Parameters.AddWithValue("?userid", senderID);
			cmd.Parameters.AddWithValue("?status", (int)Status.PENDING_STATUS);	//pending
			cmd.Parameters.AddWithValue("?tranid", transactionID.ToString());

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Withdraw money error: {0}", e.ToString());
				return false;
			}

			return bRet;
		}

		/// <summary>
		/// Here we'll make a withdraw from the sender and update transaction status to success.
		/// </summary>
		/// <param name="fromID"></param>
		/// <param name="toID"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public bool systemWithdrawMoney(UUID transactionID,string senderID, int amount)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "BEGIN;";
			sql += "UPDATE " + Table_of_Balance + " SET balance = balance - ?amount where user = ?userid;";
			sql += "UPDATE " + Table_of_Transaction + " SET status = ?status where UUID  = ?tranid;";
			sql += "COMMIT;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);

			cmd.Parameters.AddWithValue("?amount", amount);
			cmd.Parameters.AddWithValue("?userid", senderID);
			cmd.Parameters.AddWithValue("?status", (int)Status.SUCCESS_STATUS);
			cmd.Parameters.AddWithValue("?tranid", transactionID.ToString());

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Withdraw money error: {0}", e.ToString());
				return false;
			}

			return bRet;
		}

		/// <summary>
		/// Give money to the receiver and change the transaction status to success.
		/// </summary>
		/// <param name="transactionID"></param>
		/// <param name="receiverID"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		public bool giveMoney(UUID transactionID, string receiverID, int amount)
		{
			string sql = string.Empty;
			bool bRet  = false;

			sql += "BEGIN;";
			sql += "UPDATE " + Table_of_Balance + " SET balance = balance + ?amount where user = ?userid;";
			sql += "UPDATE " + Table_of_Transaction + " SET status = ?status where UUID  = ?tranid;";
			sql += "COMMIT;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);

			cmd.Parameters.AddWithValue("?amount", amount);
			cmd.Parameters.AddWithValue("?userid", receiverID);
			cmd.Parameters.AddWithValue("?status", (int)Status.SUCCESS_STATUS);//Success
			cmd.Parameters.AddWithValue("?tranid", transactionID.ToString());

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Give money error: {0}", e.ToString());
				return false;
			}

			return bRet;
		}



		///////////////////////////////////////////////////////////////////////
		//
		// transaction
		//
		public bool addTransaction(TransactionData transaction)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "INSERT INTO " + Table_of_Transaction;
			sql += " (`UUID`,`sender`,`receiver`,`amount`,`objectUUID`,`regionHandle`,`type`,`time`,`secure`,`status`,`commonName`,`description`) VALUES";
			sql += " (?transID,?sender,?receiver,?amount,?objID,?regionHandle,?type,?time,?secure,?status,?cname,?desc)";

			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?transID", transaction.TransUUID.ToString());
			cmd.Parameters.AddWithValue("?sender", transaction.Sender);
			cmd.Parameters.AddWithValue("?receiver", transaction.Receiver);
			cmd.Parameters.AddWithValue("?amount", transaction.Amount);
			cmd.Parameters.AddWithValue("?objID", transaction.ObjectUUID);
			cmd.Parameters.AddWithValue("?regionHandle", transaction.RegionHandle);
			cmd.Parameters.AddWithValue("?type", transaction.Type);
			cmd.Parameters.AddWithValue("?time", transaction.Time);
			cmd.Parameters.AddWithValue("?secure", transaction.SecureCode);
			cmd.Parameters.AddWithValue("?status", transaction.Status);
			cmd.Parameters.AddWithValue("?cname", transaction.CommonName);
			cmd.Parameters.AddWithValue("?desc", transaction.Description);

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Error adding transation to DB: {0}", e.ToString());
				return false;
			}
			return bRet;
		}


		public bool updateTransactionStatus(UUID transactionID, int status, string description)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "UPDATE " + Table_of_Transaction + " SET status = ?status,description = ?desc where UUID  = ?tranid;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?status", status);
			cmd.Parameters.AddWithValue("?desc", description);
			cmd.Parameters.AddWithValue("?tranid", transactionID);

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Error updating transation in DB: {0}", e.ToString());
				return false;
			}
			return bRet;
		}


		public bool SetTransExpired(int deadTime)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "UPDATE " + Table_of_Transaction;
			sql += " SET status = ?failedstatus,description = ?desc where time <= ?deadTime and status = ?pendingstatus;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?failedstatus", (int)Status.FAILED_STATUS);
			cmd.Parameters.AddWithValue("?desc", "expired");
			cmd.Parameters.AddWithValue("?deadTime", deadTime);
			cmd.Parameters.AddWithValue("?pendingstatus", (int)Status.PENDING_STATUS);

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Error updating transation in DB: {0}", e.ToString());
				return false;
			}
			return bRet;
		}


		/// <summary>
		/// Validate if the transacion is legal
		/// </summary>
		/// <param name="userID"></param>
		/// <param name="transactionID"></param>
		/// <returns></returns>
		public bool ValidateTransfer(string secureCode, UUID transactionID)
		{
			bool bRet = false;
			string secure = string.Empty;
			string sql = string.Empty;

			sql += "SELECT secure from " + Table_of_Transaction + " where UUID = ?transID;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?transID", transactionID.ToString());
			using (MySqlDataReader r = cmd.ExecuteReader())
			{
				if(r.Read())
				{
					try
					{
						secure = (string)r["secure"];
					}
					catch (Exception e)
					{
                        m_log.ErrorFormat("Get transaction from DB failed: {0}", e.ToString());
						return false;
					}
					if (secureCode == secure) bRet = true;
					else bRet = false;
				}
				r.Close();
			}
			return bRet;
		}


		public TransactionData FetchTransaction(UUID transactionID)
		{
			TransactionData transactionData = new TransactionData();
			transactionData.TransUUID = transactionID;
			string sql = string.Empty;

			sql += "SELECT * from " + Table_of_Transaction + " where UUID = ?transID;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?transID", transactionID.ToString());

			using (MySqlDataReader r = cmd.ExecuteReader())
			{
				if (r.Read())
				{
					try
					{
						transactionData.Sender = (string)r["sender"];
						transactionData.Receiver = (string)r["receiver"];
						transactionData.Amount = Convert.ToInt32(r["amount"]);
						transactionData.ObjectUUID = (string)r["objectUUID"];
						transactionData.RegionHandle = (string)r["regionHandle"];
						transactionData.Type = Convert.ToInt32(r["type"]);
						transactionData.Time = Convert.ToInt32(r["time"]);
						transactionData.Status = Convert.ToInt32(r["status"]);
						transactionData.CommonName  = (string)r["commonName"];
						transactionData.Description = (string)r["description"];
					}
					catch (Exception e)
					{
                        m_log.ErrorFormat("Fetching transaction failed: {0}", e.ToString());
						return null;
					}

				}
				r.Close();
			}

			return transactionData;
		}



		public TransactionData[] FetchTransaction(string userID, int startTime, int endTime, uint index, uint retNum)
		{
			List<TransactionData> rows = new List<TransactionData>();
			string sql = string.Empty;

			sql += "SELECT * from " + Table_of_Transaction + " where time>=?start AND time<=?end ";
			sql += "AND (sender=?user or receiver=?user) order by time asc limit ?index,?num;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?start", startTime);
			cmd.Parameters.AddWithValue("?end", endTime);
			cmd.Parameters.AddWithValue("?user", userID);
			cmd.Parameters.AddWithValue("?index", index);
			cmd.Parameters.AddWithValue("?num", retNum);

			using (MySqlDataReader r = cmd.ExecuteReader())
			{
				for (int i = 0; i < retNum; i++)
				{
					if (r.Read())
					{
						try
						{
							TransactionData transactionData = new TransactionData();
							string uuid = (string)r["UUID"];
							UUID transUUID;
							UUID.TryParse(uuid,out transUUID);

							transactionData.TransUUID = transUUID;
							transactionData.Sender = (string)r["sender"];
							transactionData.Receiver = (string)r["receiver"];
							transactionData.Amount = Convert.ToInt32(r["amount"]);
							if (r["objectUUID"] is System.DBNull)
							{
								transactionData.ObjectUUID = "null";
							}
							else
							{
								transactionData.ObjectUUID = (string)r["objectUUID"];
							}
							transactionData.Type = Convert.ToInt32(r["type"]);
							transactionData.Time = Convert.ToInt32(r["time"]);
							transactionData.Status = Convert.ToInt32(r["status"]);
							transactionData.CommonName  = (string)r["commonName"];
							transactionData.Description = (string)r["description"];
							rows.Add(transactionData);
						}

						catch (Exception e)
						{
                            m_log.ErrorFormat("Fetching transaction failed: {0}", e.ToString());
							return null;
						}

					}
				}
				r.Close();
			}

			cmd.Dispose();
			return rows.ToArray();
		}


		public int getTransactionNum(string userID, int startTime, int endTime)
		{
			int iRet = -1;
			string sql = string.Empty;

			sql += "SELECT COUNT(*) AS number FROM " + Table_of_Transaction + " WHERE time>=?start AND time<=?end ";
			sql += "AND (sender=?user OR receiver=?user);";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?start", startTime);
			cmd.Parameters.AddWithValue("?end", endTime);
			cmd.Parameters.AddWithValue("?user", userID);

			using (MySqlDataReader r = cmd.ExecuteReader())
			{
				if(r.Read())
				{
					try
					{
						iRet = Convert.ToInt32(r["number"]);
					}
					catch (Exception e)
					{
                        m_log.ErrorFormat("Unable to get transaction info: {0}", e.ToString());
						return -1;
					}
				}
			}
			cmd.Dispose();
			return iRet;
		}



		///////////////////////////////////////////////////////////////////////
		//
		// userinfo
		//
		public bool addUserInfo(UserInfo userInfo)
		{
			bool bRet = false;
			string sql = string.Empty;
		   
			if (userInfo.Avatar==null) return false;

			sql += "INSERT INTO " + Table_of_UserInfo + "(`user`,`simip`,`avatar`,`pass`) VALUES";
			sql += "(?user,?simip,?avatar,?password);";

			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?user", userInfo.UserID);
			cmd.Parameters.AddWithValue("?simip", userInfo.SimIP);
			cmd.Parameters.AddWithValue("?avatar", userInfo.Avatar);
			cmd.Parameters.AddWithValue("?password", userInfo.PswHash);

			try
			{
				if (cmd.ExecuteNonQuery() > 0) bRet = true;
				cmd.Dispose();
			}
			catch (Exception e)
			{
                m_log.ErrorFormat("Unable to add user information to database: {0}", e.ToString());
				bRet = false;
			}

			return bRet;
		}


		public UserInfo fetchUserInfo(string userID)
		{
			UserInfo userInfo = new UserInfo();
			userInfo.UserID = userID;
			string sql = string.Empty;

			sql += "SELECT * from " + Table_of_UserInfo + " WHERE user = ?userID;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?userID", userID);

			using (MySqlDataReader r = cmd.ExecuteReader())
			{
				if (r.Read())
				{
					try
					{
						userInfo.SimIP = (string)r["simip"];
						userInfo.Avatar = (string)r["avatar"];
						userInfo.PswHash = (string)r["pass"];
					}
					catch (Exception e)
					{
                        m_log.ErrorFormat("Fetching UserInfo failed: {0}", e.ToString());
						return null;
					}
				}
				else
				{
					return null;
				}
				r.Close();
			}

			return userInfo;
		}


		public bool updateUserInfo(UserInfo user)
		{
			bool bRet = false;
			string sql = string.Empty;

			sql += "UPDATE " + Table_of_UserInfo + " SET simip=?simip,avatar=?avatar,pass=?pass WHERE user=?user;";
			MySqlCommand cmd = new MySqlCommand(sql, m_connection);
			cmd.Parameters.AddWithValue("?simip", user.SimIP);
			cmd.Parameters.AddWithValue("?avatar", user.Avatar);
			cmd.Parameters.AddWithValue("?pass", user.PswHash);
			cmd.Parameters.AddWithValue("?user", user.UserID);

			if (cmd.ExecuteNonQuery() > 0) bRet = true;
			else bRet = false;
			cmd.Dispose();

			return bRet;
		}
	}
}
