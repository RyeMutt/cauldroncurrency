/*
 * Copyright (c) Contributors, http://opensimulator.org/, http://www.nsl.tuis.ac.jp/
 * See CONTRIBUTORS.TXT for a full list of copyright holders.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *	 * Redistributions of source code must retain the above copyright
 *	   notice, this list of conditions and the following disclaimer.
 *	 * Redistributions in binary form must reproduce the above copyright
 *	   notice, this list of conditions and the following disclaimer in the
 *	   documentation and/or other materials provided with the distribution.
 *	 * Neither the name of the OpenSim Project nor the
 *	   names of its contributors may be used to endorse or promote products
 *	   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using log4net;
using Nini.Config;
using Nwc.XmlRpc;
using Mono.Addins;

using OpenMetaverse;

using OpenSim.Framework;
using OpenSim.Framework.Servers;
using OpenSim.Framework.Servers.HttpServer;
using OpenSim.Services.Interfaces;
using OpenSim.Region.Framework;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;

using NSL.Certificate.Tools;
using NSL.Network.XmlRpc;


[assembly: Addin("CauldronMoneyModule", "0.10")]
[assembly: AddinDependency("OpenSim.Region.Framework", OpenSim.VersionInfo.VersionNumber)]


namespace Cauldron.Addons.Money
{
	//
	public enum TransactionType : int
	{
		// One-Time Charges
		GroupCreate	 	= 1002,
		GroupJoin		= 1004,
		UploadCharge	= 1101,
		LandAuction		= 1102,
		ClassifiedCharge= 1103,
		// Recurrent Charges
		ParcelDirFee	= 2003,
		ClassifiedRenew = 2005,
		ScheduledFee	= 2900,
		// Inventory Transactions
		GiveInventory   = 3000,
		// Transfers Between Users
		ObjectSale		= 5000,
		Gift			= 5001,
		LandSale		= 5002,
		ReferBonus		= 5003,
		InventorySale	= 5004,
		RefundPurchase	= 5005,
		LandPassSale	= 5006,
		DwellBonus		= 5007,
		PayObject		= 5008,
		ObjectPays		= 5009,
		BuyMoney		= 5010,
		MoveMoney		= 5011,
		// Group Transactions
		GroupLiability  = 6003,
		GroupDividend   = 6004,

		LindenAdjustment = 9000,
		// Stipend Credits
		YrGridWithdrawl  = 10000, /* Formerly StipendPayment */
        StipendDelta    = 10005 
	}

/*
	public enum OpenMetaverse.MoneyTransactionType : int
	{
		None = 0,
		FailSimulatorTimeout = 1,
		FailDataserverTimeout = 2,
		ObjectClaim = 1000,
		LandClaim = 1001,
		GroupCreate = 1002,
		ObjectPublicClaim = 1003,
		GroupJoin = 1004,
		TeleportCharge = 1100,
		UploadCharge = 1101,
		LandAuction = 1102,
		ClassifiedCharge = 1103,
		ObjectTax = 2000,
		LandTax = 2001,
		LightTax = 2002,
		ParcelDirFee = 2003,
		GroupTax = 2004,
		ClassifiedRenew = 2005,
		GiveInventory = 3000,
		ObjectSale = 5000,
		Gift = 5001,
		LandSale = 5002,
		ReferBonus = 5003,
		InventorySale = 5004,
		RefundPurchase = 5005,
		LandPassSale = 5006,
		DwellBonus = 5007,
		PayObject = 5008,
		ObjectPays = 5009,
		GroupLandDeed = 6001,
		GroupObjectDeed = 6002,
		GroupLiability = 6003,
		GroupDividend = 6004,
		GroupMembershipDues = 6005,
		ObjectRelease = 8000,
		LandRelease = 8001,
		ObjectDelete = 8002,
		ObjectPublicDecay = 8003,
		ObjectPublicDelete = 8004,
		LindenAdjustment = 9000,
		LindenGrant = 9001,
		LindenPenalty = 9002,
		EventFee = 9003,
		EventPrize = 9004,
		StipendBasic = 10000,
		StipendDeveloper = 10001,
		StipendAlways = 10002,
		StipendDaily = 10003,
		StipendRating = 10004,
		StipendDelta = 10005
	}
*/

	[Extension(Path = "/OpenSim/RegionModules", NodeName = "RegionModule", Id = "MoneyModule")]
	public class MoneyModule : IMoneyModule, ISharedRegionModule
	{
		// Private data members.   
		private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string TIMEOUT_OCCURED = "Timeout occured";

		private bool m_sellEnabled = false;

		private IConfigSource m_config;

		private string m_moneyServURL	 = string.Empty;
		public  BaseHttpServer HttpServer;

		private string m_certFilename	 = "";
		private string m_certPassword	 = "";
		private bool   m_checkServerCert = false;
		private string m_cacertFilename	 = "";
		private X509Certificate2 m_cert	 = null;

        private int    m_RequestTimeout = 10000;

		private NSLCertificateVerify m_certVerify = new NSLCertificateVerify();	// Authentication Server


		/// <summary>   
		/// Scene dictionary indexed by Region Handle   
		/// </summary>   
		private Dictionary<ulong, Scene> m_sceneList = new Dictionary<ulong, Scene>();

		/// <summary>   
		/// To cache the balance data while the money server is not available.   
		/// </summary>   
		private Dictionary<UUID, int> m_moneyServer = new Dictionary<UUID, int>();

		// Events  
		public event ObjectPaid OnObjectPaid;

		// Price
		private int   ObjectCount 			  = 0;
		private int   PriceEnergyUnit 		  = 0;
		private int   PriceGroupCreate 		  = 0;
		private int   PriceObjectClaim 		  = 0;
		private float PriceObjectRent 		  = 0f;
		private float PriceObjectScaleFactor  = 0f;
		private int   PriceParcelClaim 		  = 0;
		private int   PriceParcelRent		  = 0;
		private float PriceParcelClaimFactor  = 0f;
		private int   PricePublicObjectDecay  = 0;
		private int   PricePublicObjectDelete = 0;
		private int   PriceRentLight 		  = 0;
		private int   PriceUpload 			  = 0;
		private int   TeleportMinPrice 		  = 0;
		private float TeleportPriceExponent   = 0f;
		private float EnergyEfficiency 		  = 0f;

		//

		#region ISharedRegionModule interface

		public void Initialise(IConfigSource source)
		{
			// Handle the parameters errors.
			if (source == null) return;

			try
			{
				m_config = source;

				// [Economy] section
				IConfig economyConfig = m_config.Configs["Economy"];

				if (economyConfig.GetString("EconomyModule") != Name)
				{
					m_log.InfoFormat("[MONEY] Disabled");
					return;
				}
				else
				{
					m_log.InfoFormat("[MONEY] Enabled");
				}

				m_sellEnabled  = economyConfig.GetBoolean("SellEnabled", false);
				m_moneyServURL = economyConfig.GetString("CurrencyServer");

				m_certFilename = economyConfig.GetString("ClientCertFilename", "");
				m_certPassword = economyConfig.GetString("ClientCertPassword", "");
                if (!String.IsNullOrWhiteSpace(m_certFilename)) 
                {
					m_cert = new X509Certificate2(m_certFilename, m_certPassword);
					m_log.InfoFormat("[MONEY] Issue client authentication. Cert File is " + m_certFilename);
				}
					
				string checkcert = economyConfig.GetString("CheckServerCert", "false");
				if (checkcert.ToLower() == "true") m_checkServerCert = true;

				m_cacertFilename = economyConfig.GetString("CACertFilename", "");
                if (!String.IsNullOrWhiteSpace(m_cacertFilename)) 
                {
					m_certVerify.SetPrivateCA(m_cacertFilename);
					m_log.InfoFormat("[MONEY] Executing Authentication Server. CA Cert File is " + m_cacertFilename);
				}
				else 
                {
					m_checkServerCert = false;
				}
				ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(m_certVerify.ValidateServerCertificate);

                m_RequestTimeout = economyConfig.GetInt("RequestTimeout", m_RequestTimeout);

				// Price
				PriceEnergyUnit 		= economyConfig.GetInt	("PriceEnergyUnit", 		100);
				PriceObjectClaim 		= economyConfig.GetInt	("PriceObjectClaim", 		10);
				PricePublicObjectDecay 	= economyConfig.GetInt	("PricePublicObjectDecay", 	4);
				PricePublicObjectDelete = economyConfig.GetInt	("PricePublicObjectDelete", 4);
				PriceParcelClaim 		= economyConfig.GetInt	("PriceParcelClaim", 		1);
				PriceParcelClaimFactor 	= economyConfig.GetFloat("PriceParcelClaimFactor", 	1f);
				PriceUpload 			= economyConfig.GetInt	("PriceUpload", 			0);
				PriceRentLight			= economyConfig.GetInt	("PriceRentLight",			5);
				PriceObjectRent 		= economyConfig.GetFloat("PriceObjectRent", 		1);
				PriceObjectScaleFactor 	= economyConfig.GetFloat("PriceObjectScaleFactor", 	10);
				PriceParcelRent 		= economyConfig.GetInt	("PriceParcelRent", 		1);
				PriceGroupCreate 		= economyConfig.GetInt	("PriceGroupCreate", 		0);
				TeleportMinPrice 		= economyConfig.GetInt	("TeleportMinPrice", 		2);
				TeleportPriceExponent 	= economyConfig.GetFloat("TeleportPriceExponent", 	2f);
				EnergyEfficiency 		= economyConfig.GetFloat("EnergyEfficiency", 		1);

			}
			catch
			{
				m_log.ErrorFormat("[MONEY] Failed to read configuration file");
			}
		}



		public void AddRegion(Scene scene)
		{
			scene.RegisterModuleInterface<IMoneyModule>(this);

			lock (m_sceneList)
			{
				if (m_sceneList.Count == 0)
				{
					if (!string.IsNullOrEmpty(m_moneyServURL))
					{
						HttpServer = new BaseHttpServer(9000);
						HttpServer.AddStreamHandler(new RegionStatsHandler(scene.RegionInfo));

						HttpServer.AddXmlRPCHandler("OnMoneyTransfered", OnMoneyTransferedHandler);
						HttpServer.AddXmlRPCHandler("UpdateBalance", BalanceUpdateHandler);
						HttpServer.AddXmlRPCHandler("UserAlert", UserAlertHandler);
						HttpServer.AddXmlRPCHandler("GetBalance", GetBalanceHandler);
						HttpServer.AddXmlRPCHandler("SendMoneyBalance", SendMoneyBalanceHandler);

						MainServer.Instance.AddXmlRPCHandler("OnMoneyTransfered", OnMoneyTransferedHandler);
						MainServer.Instance.AddXmlRPCHandler("UpdateBalance", BalanceUpdateHandler);
						MainServer.Instance.AddXmlRPCHandler("UserAlert", UserAlertHandler);
						MainServer.Instance.AddXmlRPCHandler("GetBalance", GetBalanceHandler);
						MainServer.Instance.AddXmlRPCHandler("SendMoneyBalance", SendMoneyBalanceHandler);
					}
				}

				if (m_sceneList.ContainsKey(scene.RegionInfo.RegionHandle))
				{
					m_sceneList[scene.RegionInfo.RegionHandle] = scene;
				}
				else
				{
					m_sceneList.Add(scene.RegionInfo.RegionHandle, scene);
				}

			}

			scene.EventManager.OnNewClient			+= OnNewClient;
			scene.EventManager.OnMakeRootAgent		+= OnMakeRootAgent;
			scene.EventManager.OnMakeChildAgent		+= MakeChildAgent;
			scene.EventManager.OnMoneyTransfer		+= MoneyTransferAction;
			scene.EventManager.OnValidateLandBuy	+= ValidateLandBuy;
			scene.EventManager.OnLandBuy 			+= processLandBuy;
		}

	

		public void RemoveRegion(Scene scene)
		{
		}


		public void RegionLoaded(Scene scene)
		{
		}


		public Type ReplaceableInterface
		{
			get { return null; }
		}


		public bool IsSharedModule
		{
			get { return true; }
		}


		public string Name
		{
			get { return "CauldronMoneyModule"; }
		}


		public void PostInitialise()
		{
		}


		public void Close()
		{
		}

		#endregion


		#region IMoneyModule interface.

		// for LSL llGiveMoney() function
        public ETransferMoneyReturn ObjectGiveMoney(UUID objectID, UUID fromID, UUID toID, int amount, UUID transactionID)
		{
			if (!m_sellEnabled) return ETransferMoneyReturn.DISABLED;

            string objectName = string.Empty;
            string senderName = "Unknown";
            string receiverName = "Unknown";
            string region = "Unknown";

			SceneObjectPart sceneObj = GetLocatePrim(objectID);
			if (sceneObj != null)
			{
                objectName = sceneObj.ParentGroup.Name;
			}

			Scene scene = GetLocateScene(toID);
			if (scene != null)
			{
                senderName = scene.UserManagementModule.GetUserName(fromID);
                receiverName = scene.UserManagementModule.GetUserName(toID);
			}

            ETransferMoneyReturn ret = ETransferMoneyReturn.FAILURE;
            string description = String.Format("Source: {0} Object pays {1} Region: {2} Description: {3}",
                                               senderName, receiverName, region, objectName);

			if (sceneObj.OwnerID == fromID)
			{
				ulong regionHandle = sceneObj.RegionHandle;
				if (GetLocateClient(fromID) != null)
				{
                    ret = TransferMoney(fromID, toID, amount, (int)MoneyTransactionType.ObjectPays, objectID, regionHandle, description, transactionID);
				}
				else
				{
                    ret = ForceTransferMoney(fromID, toID, amount, (int)MoneyTransactionType.ObjectPays, objectID, regionHandle, description, transactionID);
				}
			}

			return ret;
		}


		//
		public int UploadCharge
		{
			get { return PriceUpload; }
		}


		//
		public int GroupCreationCharge
		{
			get { return PriceGroupCreate; }
		}


		public int GetBalance(UUID agentID)
		{
			IClientAPI client = GetLocateClient(agentID);
			return QueryBalanceFromMoneyServer(client);
		}


		public bool UploadCovered(UUID agentID, int amount)
		{
			IClientAPI client = GetLocateClient(agentID);
			int balance = QueryBalanceFromMoneyServer(client);

            return balance >= amount;
		}


		public bool AmountCovered(UUID agentID, int amount)
		{
			IClientAPI client = GetLocateClient(agentID);
			int balance = QueryBalanceFromMoneyServer(client);

            return balance >= amount;
		}


		public void ApplyUploadCharge(UUID agentID, int amount, string text)
		{
			ulong region = GetLocateScene(agentID).RegionInfo.RegionHandle;
			PayMoneyCharge(agentID, amount, MoneyTransactionType.UploadCharge, region, text);
		}

        public bool ApplyCharge(UUID agentID, int amount, MoneyTransactionType type, string text = "")
		{
			ulong region = GetLocateScene(agentID).RegionInfo.RegionHandle;
            return (PayMoneyCharge(agentID, amount, type, region, text) == ETransferMoneyReturn.SUCCESS);
		}
		#endregion




		#region MoneyModule event handlers

		// 
		private void OnNewClient(IClientAPI client)
		{
			client.OnEconomyDataRequest += OnEconomyDataRequest;
		}


		public void OnMakeRootAgent(ScenePresence agent)
		{
			int balance = 0;
			IClientAPI client = agent.ControllingClient;

			LoginMoneyServer(client, out balance);

            LandCreditStruct landData;
            Scene scene = GetLocateScene(client.AgentId);
            if (scene != null && scene.GridLandService != null)
            {
                landData = scene.GridLandService.GetLandCredit(client.AgentId);
            }
            else
            {
                landData = new LandCreditStruct(0, 0);
                m_log.DebugFormat("[MONEY] OnMoneyBalanceRequest: No Scene. Cannot return land credit data.");
            }

			client.SendMoneyBalance(UUID.Zero, true, new byte[0], balance, 0, UUID.Zero, false, UUID.Zero, 
                                    false, 0, String.Empty, landData.Credit, landData.Committed);

			client.OnMoneyBalanceRequest 	+= OnMoneyBalanceRequest;
			client.OnRequestPayPrice 		+= OnRequestPayPrice;
			client.OnObjectBuy				+= OnObjectBuy;
			client.OnLogout 				+= ClientClosed;
		}	   


		// for OnClientClosed event
		private void ClientClosed(IClientAPI client)
		{
			if (client != null)
			{
				LogoffMoneyServer(client);
			}
		}


		// for OnMoneyTransfer event
		private void MoneyTransferAction(Object sender, EventManager.MoneyTransferArgs moneyEvent)
		{
			if (!m_sellEnabled) return;

			// Check that a transaction is necessary.   
			if (moneyEvent.sender == moneyEvent.receiver) return;

			UUID receiver = moneyEvent.receiver;
			if (moneyEvent.transactiontype == (int)MoneyTransactionType.PayObject)		// Pay for the object.   
			{
				SceneObjectPart sceneObj = GetLocatePrim(moneyEvent.receiver);
                if (sceneObj == null) 
                    return;
                else
                    receiver = sceneObj.OwnerID;
			}

			// Before paying for the object, save the object local ID for current transaction.
			UUID  objectID = UUID.Zero;
			ulong regionHandle = 0;

            string senderName = "Unknown";
            string receiverName = "Unknown";
            string region = "Unknown";
			if (sender is Scene)
			{
				Scene scene  = (Scene)sender;
				regionHandle = scene.RegionInfo.RegionHandle;

                senderName = scene.UserManagementModule.GetUserName(moneyEvent.sender);

                if (moneyEvent.transactiontype == (int)MoneyTransactionType.PayObject)
                {
                    SceneObjectPart obj = GetLocatePrim(moneyEvent.receiver);
                    objectID = obj.UUID;
                    receiverName = obj.ParentGroup.Name;
                }
                else
                {
                    receiverName = scene.UserManagementModule.GetUserName(moneyEvent.receiver);
                }
                region = scene.RegionInfo.RegionName;
			}

            TransferMoney(moneyEvent.sender, receiver, moneyEvent.amount, moneyEvent.transactiontype, 
                          objectID, regionHandle, String.Format("{0} pays {1} Region: {2}",
                    senderName, receiverName, region), UUID.Zero);
		}



		// for OnMakeChildAgent event
		private void MakeChildAgent(ScenePresence avatar)
		{
		}


		// for OnValidateLandBuy event
		private void ValidateLandBuy(Object sender, EventManager.LandBuyArgs landBuyEvent)
		{	
			IClientAPI senderClient = GetLocateClient(landBuyEvent.agentId);
			if (senderClient != null)
			{
				int balance = QueryBalanceFromMoneyServer(senderClient);
				if (balance >= landBuyEvent.parcelPrice)
				{
					lock(landBuyEvent)
					{
						landBuyEvent.economyValidated = true;
					}
				}
			}
		}



		// for LandBuy event
		private void processLandBuy(Object sender, EventManager.LandBuyArgs landBuyEvent)
		{
			if (!m_sellEnabled) return;

			lock(landBuyEvent)
			{
				if (landBuyEvent.economyValidated==true && landBuyEvent.transactionID==0)
				{
					landBuyEvent.transactionID = Util.UnixTimeSinceEpoch();

					ulong parcelID = (ulong)landBuyEvent.parcelLocalID;
					UUID  regionID = UUID.Zero;
                    string regionName = "Unknown";
                    if (sender is Scene)
                    {
                        regionID = ((Scene)sender).RegionInfo.RegionID;
                        regionName = ((Scene)sender).RegionInfo.RegionName;
                    }

					if (TransferMoney(landBuyEvent.agentId, landBuyEvent.parcelOwnerID, 
									  landBuyEvent.parcelPrice, (int)MoneyTransactionType.LandSale, 
                                      regionID, parcelID, String.Format("Land Purchase in {0}", regionName), 
                                      UUID.Zero) == ETransferMoneyReturn.SUCCESS)
					{
						landBuyEvent.amountDebited = landBuyEvent.parcelPrice;
					}
				}
			}
		}


		// for OnObjectBuy event
		public void OnObjectBuy(IClientAPI remoteClient, UUID agentID, UUID sessionID, 
								UUID groupID, UUID categoryID, uint localID, byte saleType, int salePrice)
		{
			// Handle the parameters error.   
			if (!m_sellEnabled) return;
			if (remoteClient == null || salePrice < 0) return;		// for L$0 Sell

			// Get the balance from money server.   
			int balance = QueryBalanceFromMoneyServer(remoteClient);
			if (balance < salePrice)
			{
				remoteClient.SendAgentAlertMessage("Unable to buy. You have insufficient funds", false);
				return;
			}

			Scene scene = GetLocateScene(remoteClient.AgentId);
			if (scene != null)
			{
				SceneObjectPart sceneObj = scene.GetSceneObjectPart(localID);
				if (sceneObj != null)
				{
					IBuySellModule mod = scene.RequestModuleInterface<IBuySellModule>();
					if (mod != null)
					{
						UUID receiverId = sceneObj.OwnerID;
						ulong regionHandle = sceneObj.RegionHandle;

                        string receiverName = scene.UserManagementModule.GetUserName(receiverId);
                        string description = String.Format("Destination: {0} Object Sale Region: {1} Description: {2}",
                            receiverName, scene.RegionInfo.RegionName, sceneObj.ParentGroup.Name);
						if (TransferMoney(remoteClient.AgentId, receiverId, salePrice, (int)MoneyTransactionType.PayObject, 
                                          sceneObj.UUID, regionHandle, description, UUID.Zero) == ETransferMoneyReturn.SUCCESS)
						{
							mod.BuyObject(remoteClient, categoryID, localID, saleType, salePrice);
						}
					}
				}
				else
				{
					remoteClient.SendAgentAlertMessage("Unable to buy. Could not locate the object.", false);
					return;
				}
			}
		}



		/// <summary>   
		/// Sends the the stored money balance to the client   
		/// </summary>   
		/// <param name="client"></param>   
		/// <param name="agentID"></param>   
		/// <param name="SessionID"></param>   
		/// <param name="TransactionID"></param>   
		private void OnMoneyBalanceRequest(IClientAPI client, UUID agentID, UUID SessionID, UUID TransactionID)
		{
			if (client.AgentId == agentID && client.SessionId == SessionID)
			{
                if (string.IsNullOrEmpty(m_moneyServURL))
                {
                    client.SendAlertMessage("Failed to query balance.");
                    return;
                }

                int balance = QueryBalanceFromMoneyServer(client);
                if (balance == int.MinValue)
                    client.SendAlertMessage("Failed to query balance.");
                else
                {
                    LandCreditStruct landData;
                    Scene scene = GetLocateScene(client.AgentId);
                    if (scene != null && scene.GridLandService != null)
                    {
                        landData = scene.GridLandService.GetLandCredit(client.AgentId);
                    }
                    else
                    {
                        landData = new LandCreditStruct(0, 0);
                        m_log.DebugFormat("[MONEY] OnMoneyBalanceRequest: No Scene. Cannot return land credit data.");
                    }
                    client.SendMoneyBalance(TransactionID, true, new byte[0], balance, 0, UUID.Zero, false, UUID.Zero, false, 0, String.Empty,
                        landData.Credit, landData.Committed);
                }
			}
			else
			{
				client.SendAlertMessage("An error occured while sending your balance.");
			}
		}



		private void OnRequestPayPrice(IClientAPI client, UUID objectID)
		{
			Scene scene = GetLocateScene(client.AgentId);
			if (scene==null) return;

			SceneObjectPart sceneObj = scene.GetSceneObjectPart(objectID);
			if (sceneObj==null) return;

			SceneObjectGroup group = sceneObj.ParentGroup;
			SceneObjectPart root = group.RootPart;

			client.SendPayPrice(objectID, root.PayPrice);
		}


		private void OnEconomyDataRequest(IClientAPI user)
		{
			if (user!=null)
			{
				Scene s = (Scene)user.Scene;

				user.SendEconomyData(EnergyEfficiency, s.RegionInfo.ObjectCapacity, ObjectCount, PriceEnergyUnit, PriceGroupCreate,
									 PriceObjectClaim, PriceObjectRent, PriceObjectScaleFactor, PriceParcelClaim, PriceParcelClaimFactor,
									 PriceParcelRent, PricePublicObjectDecay, PricePublicObjectDelete, PriceRentLight, PriceUpload,
									 TeleportMinPrice, TeleportPriceExponent);
			}
		}

		#endregion



		#region MoneyModule XML-RPC Handler

		// "OnMoneyTransfered" RPC from MoneyServer
		public XmlRpcResponse OnMoneyTransferedHandler(XmlRpcRequest request, IPEndPoint remoteClient)
		{
			bool ret = false;

			if (request.Params.Count > 0)
			{
				Hashtable requestParam = (Hashtable)request.Params[0];
				if (requestParam.Contains("clientUUID") &&
					requestParam.Contains("clientSessionID") &&
					requestParam.Contains("clientSecureSessionID"))
				{
					UUID clientUUID = UUID.Zero;
					UUID.TryParse((string)requestParam["clientUUID"], out clientUUID);
					if (clientUUID != UUID.Zero)
					{
						IClientAPI client = GetLocateClient(clientUUID);
						if (client != null &&
                            client.SessionId.ToString() == requestParam["clientSessionID"].ToString() &&
                            client.SecureSessionId.ToString() == requestParam["clientSecureSessionID"].ToString())
						{
							if (requestParam.Contains("transactionType") &&
								requestParam.Contains("objectID") &&
								requestParam.Contains("amount"))
							{
                                if (int.Parse(requestParam["transactionType"].ToString()) == (int)MoneyTransactionType.PayObject)		// Pay for the object.
								{
									// Send notify to the client(viewer) for Money Event Trigger.   
									ObjectPaid handlerOnObjectPaid = OnObjectPaid;
									if (handlerOnObjectPaid != null)
									{
										UUID objectID = UUID.Zero;
                                        UUID.TryParse(requestParam["objectID"].ToString(), out objectID);
										handlerOnObjectPaid(objectID, clientUUID, (int)requestParam["amount"]);	// call Script Engine for LSL money()
									}
									ret = true;
								}
							}
						}
					}
				}
			}

			// Send the response to money server.
			XmlRpcResponse resp   = new XmlRpcResponse();
			Hashtable paramTable  = new Hashtable();
			paramTable["success"] = ret;

			if (!ret)
			{
				m_log.ErrorFormat("[MONEY] OnMoneyTransferedHandler: Transaction failed. MoneyServer will rollback");
			}
			resp.Value = paramTable;

			return resp;
		}



		// "UpdateBalance" RPC from MoneyServer or Script
		public XmlRpcResponse BalanceUpdateHandler(XmlRpcRequest request, IPEndPoint remoteClient)
		{
			bool ret = false;

			#region Update the balance from money server.

			if (request.Params.Count > 0)
			{
				Hashtable requestParam = (Hashtable)request.Params[0];
				if (requestParam.Contains("clientUUID") &&
					requestParam.Contains("clientSessionID") &&
					requestParam.Contains("clientSecureSessionID"))
				{
					UUID clientUUID = UUID.Zero;
					UUID.TryParse((string)requestParam["clientUUID"], out clientUUID);
					if (clientUUID != UUID.Zero)
					{
						IClientAPI client = GetLocateClient(clientUUID);
						if (client != null &&
                            client.SessionId.ToString() == requestParam["clientSessionID"].ToString() &&
                            client.SecureSessionId.ToString() == requestParam["clientSecureSessionID"].ToString())
						{
							if (requestParam.Contains("Balance"))
							{
                                UUID trans_id = UUID.Random();
                                if (requestParam.Contains("transactionID"))
                                    UUID.TryParse(requestParam["transactionID"].ToString(), out trans_id);

								UUID source_id = UUID.Zero;
								if (requestParam.Contains("sourceID"))
                                    UUID.TryParse(requestParam["sourceID"].ToString(), out source_id);

								UUID dest_id = UUID.Zero;
								if (requestParam.Contains("destID"))
                                    UUID.TryParse(requestParam["destID"].ToString(), out dest_id);

								int type = (requestParam.Contains("type")) ? (int)requestParam["type"] : 0;
								int amount = (requestParam.Contains("amount")) ? (int)requestParam["amount"] : 0;

								// Notify client.   
								string msg = String.Empty;
                                if (requestParam.Contains("Message")) 
                                    msg = requestParam["Message"].ToString();

                                LandCreditStruct landData;
                                Scene scene = GetLocateScene(client.AgentId);
                                if (scene != null && scene.GridLandService != null)
                                {
                                    landData = scene.GridLandService.GetLandCredit(client.AgentId);
                                }
                                else
                                {
                                    landData = new LandCreditStruct(0, 0);
                                    m_log.DebugFormat("[MONEY] BalanceUpdateHandler: No scene. Cannot return land credit data.");
                                }
								client.SendMoneyBalance(trans_id, true, Utils.StringToBytes(msg), (int)requestParam["Balance"],
														type, source_id, false, dest_id, false, amount, String.Empty,
                                                        landData.Credit, landData.Committed);
								ret = true;
							}
						}
					}
				}
			}

			#endregion

			// Send the response to money server.
			XmlRpcResponse resp   = new XmlRpcResponse();
			Hashtable paramTable  = new Hashtable();
			paramTable["success"] = ret;

			if (!ret)
			{
				m_log.ErrorFormat("[MONEY] BalanceUpdateHandler: Cannot update client balance from MoneyServer");
			}
			resp.Value = paramTable;

			return resp;
		}



		// "UserAlert" RPC from Script
		public XmlRpcResponse UserAlertHandler(XmlRpcRequest request, IPEndPoint remoteClient)
		{
			bool ret = false;

			#region confirm the request and show the notice from money server.

			if (request.Params.Count > 0)
			{
				Hashtable requestParam = (Hashtable)request.Params[0];
				if (requestParam.Contains("clientUUID") &&
					requestParam.Contains("clientSessionID") &&
					requestParam.Contains("clientSecureSessionID"))
				{
					UUID clientUUID = UUID.Zero;
					UUID.TryParse((string)requestParam["clientUUID"], out clientUUID);
					if (clientUUID != UUID.Zero)
					{
						IClientAPI client = GetLocateClient(clientUUID);
						if (client != null &&
							client.SessionId.ToString()==(string)requestParam["clientSessionID"] &&
							client.SecureSessionId.ToString()==(string)requestParam["clientSecureSessionID"])
						{
							if (requestParam.Contains("Description"))
							{
                                string description = requestParam["Description"].ToString();
								// Show the notice dialog with money server message.
							   	GridInstantMessage gridMsg = new GridInstantMessage(null, UUID.Zero, "MonyServer", new UUID(clientUUID.ToString()),
																	(byte)InstantMessageDialog.MessageFromAgent, description, false, new Vector3());
								client.SendInstantMessage(gridMsg);
								ret = true; 
							}
						}
					}
				}
			}
			//
			#endregion

			// Send the response to money server.
			XmlRpcResponse resp   = new XmlRpcResponse();
			Hashtable paramTable  = new Hashtable();
			paramTable["success"] = ret;

			resp.Value = paramTable;
			return resp;
		}



		// "GetBalance" RPC from Script
		public XmlRpcResponse GetBalanceHandler(XmlRpcRequest request, IPEndPoint remoteClient)
		{
			bool ret = false;
            int  balance = int.MinValue;

			if (request.Params.Count > 0)
			{
				Hashtable requestParam = (Hashtable)request.Params[0];
				if (requestParam.Contains("clientUUID") &&
					requestParam.Contains("clientSessionID") &&
					requestParam.Contains("clientSecureSessionID"))
				{
					UUID clientUUID = UUID.Zero;
					UUID.TryParse((string)requestParam["clientUUID"], out clientUUID);
					if (clientUUID!=UUID.Zero)
					{
						IClientAPI client = GetLocateClient(clientUUID);
						if (client!=null &&
							client.SessionId.ToString()==(string)requestParam["clientSessionID"] &&
							client.SecureSessionId.ToString()==(string)requestParam["clientSecureSessionID"])
						{
							balance = QueryBalanceFromMoneyServer(client);
						}
					}
				}
			}

			// Send the response to caller.
            if (balance == int.MinValue) 
			{
				m_log.ErrorFormat("[MONEY] GetBalanceHandler: GetBalance operation failed");
				ret = false;
			}

			XmlRpcResponse resp   = new XmlRpcResponse();
			Hashtable paramTable  = new Hashtable();
			paramTable["success"] = ret;
			paramTable["balance"] = balance;
			resp.Value = paramTable;

			return resp;
		}



		// "SendMoneyBalance" RPC from Script
		public XmlRpcResponse SendMoneyBalanceHandler(XmlRpcRequest request, IPEndPoint remoteClient)
		{
			bool ret = false;

			if (request.Params.Count > 0)
			{
				Hashtable requestParam = (Hashtable)request.Params[0];
				if (requestParam.Contains("clientUUID") &&
					requestParam.Contains("secretAccessCode"))
				{
					UUID clientUUID = UUID.Zero;
					UUID.TryParse((string)requestParam["clientUUID"], out clientUUID);
					if (clientUUID != UUID.Zero)
					{
						if (requestParam.Contains("amount"))
						{
							int amount  = (int)requestParam["amount"];
                            string secretCode = requestParam["secretAccessCode"].ToString();
							string scriptIP   = remoteClient.Address.ToString();

							MD5 md5 = MD5.Create();
							byte[] code = md5.ComputeHash(ASCIIEncoding.Default.GetBytes(secretCode + "_" + scriptIP));
							string hash = BitConverter.ToString(code).ToLower().Replace("-","");
							ret = SendMoneyBalance(clientUUID, amount, hash);
						}
                        else 
                        {
                            m_log.ErrorFormat("[MONEY] SendMoneyBalanceHandler: Amount parameter is missing");
                        }
					}
				}
				else 
                {
					if (!requestParam.Contains("clientUUID")) 
                    {
						m_log.ErrorFormat("[MONEY] SendMoneyBalanceHandler: clientUUID parameter is missing");
					}
					if (!requestParam.Contains("secretAccessCode")) 
                    {
						m_log.ErrorFormat("[MONEY] SendMoneyBalanceHandler: secretAccessCode is missing");
					}
				}
			}
			else 
            {
				m_log.ErrorFormat("[MONEY] SendMoneyBalanceHandler: No parameters");
			}

			if (!ret) m_log.ErrorFormat("[MONEY] SendMoneyBalanceHandler: Send Money transaction failed");

			// Send the response to caller.
			XmlRpcResponse resp   = new XmlRpcResponse();
			Hashtable paramTable  = new Hashtable();
			paramTable["success"] = ret;

			resp.Value = paramTable;

			return resp;
		}
			
		#endregion


		#region MoneyModule private help functions

		/// <summary>   
		/// Transfer the money from one user to another. Need to notify money server to update.   
		/// </summary>   
		/// <param name="amount">   
		/// The amount of money.   
		/// </param>   
		/// <returns>   
		/// return ETransferMoneyReturn.   
		/// </returns>   
        private ETransferMoneyReturn TransferMoney(UUID sender, UUID receiver, int amount, int type, UUID objectID, 
                                                   ulong regionHandle, string description, UUID transactionID)
		{
            ETransferMoneyReturn ret = ETransferMoneyReturn.FAILURE;
			IClientAPI senderClient = GetLocateClient(sender);

			// Handle illegal transactions.   
			if (senderClient == null) // receiverClient could be null.
			{
				m_log.InfoFormat("[MONEY] TransferMoney: Client {0} not found", sender.ToString());
                return ETransferMoneyReturn.INVALID_AGENT;
			}

            int balance = QueryBalanceFromMoneyServer(senderClient);
            if (balance == int.MinValue) 
            {
                return ETransferMoneyReturn.FAILURE;
            }
			else if (balance < amount)
			{
                m_log.InfoFormat("[MONEY] TransferMoney: Insufficient funds (${0}) to cover ${1} charge for client [{2}]", balance, amount, sender.ToString());
				return ETransferMoneyReturn.INSUFF_FUNDS;
			}

			#region Send transaction request to money server and parse the resultes.

			if (string.IsNullOrEmpty(m_moneyServURL))
            {
                m_log.ErrorFormat("[MONEY] TransferMoney: Money Server is not available.");
                return ETransferMoneyReturn.DISABLED;
            }
			
			// Fill parameters for money transfer XML-RPC.   
			Hashtable paramTable = new Hashtable();
			paramTable["senderID"] 				= sender.ToString();
			paramTable["receiverID"] 			= receiver.ToString();
			paramTable["senderSessionID"] 		= senderClient.SessionId.ToString();
			paramTable["senderSecureSessionID"] = senderClient.SecureSessionId.ToString();
			paramTable["transactionType"] 		= type;
			paramTable["objectID"] 				= objectID.ToString();
			paramTable["regionHandle"] 			= regionHandle.ToString();
			paramTable["amount"] 				= amount;
			paramTable["description"] 			= description;
            // We let objects generate a transaction uuid pre-flight
            if (transactionID != UUID.Zero)
                paramTable["transactionID"]  = transactionID.ToString();

			// Generate the request for transfer.   
			Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "TransferMoney");
           
			// Handle the return values from Money Server.  
			if (resultTable != null && resultTable.Contains("success"))
			{
                if (bool.Parse(resultTable["success"].ToString()))
				{
					ret = ETransferMoneyReturn.SUCCESS;
				}
                else
                {
                    if (resultTable.Contains("errorMessage")
                        && resultTable["errorMessage"].ToString() == TIMEOUT_OCCURED)
                    {
                        ret = ETransferMoneyReturn.EXPIRED;
                    }   
                }
			}
			else 
            {
                m_log.ErrorFormat("[MONEY] TransferMoney: Can not money transfer request from [{0}] to [{1}]", sender.ToString(), receiver.ToString());
            }

			#endregion

			return ret;
		}



		/// <summary>   
		/// Force transfer the money from one user to another. 
		/// This function does not check that the sender is logged in.
		/// Need to notify money server to update.   
		/// </summary>   
		/// <param name="amount">   
		/// The amount of money.   
		/// </param>   
		/// <returns>   
        /// return ETransferMoneyReturn   
		/// </returns>   
		private ETransferMoneyReturn ForceTransferMoney(UUID sender, UUID receiver, int amount, int type,  UUID objectID, 
                                                        ulong regionHandle, string description, UUID transactionID)
		{
            ETransferMoneyReturn ret = ETransferMoneyReturn.FAILURE;

			#region Force send transaction request to money server and parse the results.

			if (!string.IsNullOrEmpty(m_moneyServURL))
			{
				// Fill parameters for money transfer XML-RPC.   
				Hashtable paramTable = new Hashtable();
				paramTable["senderID"] 			 = sender.ToString();
				paramTable["receiverID"] 		 = receiver.ToString();
				paramTable["transactionType"] 	 = type;
				paramTable["objectID"] 			 = objectID.ToString();
				paramTable["regionHandle"] 		 = regionHandle.ToString();
				paramTable["amount"] 			 = amount;
				paramTable["description"] 		 = description;
                // We let objects generate a transaction uuid pre-flight
                if (transactionID != UUID.Zero)
                    paramTable["transactionID"]  = transactionID.ToString();

				// Generate the request for transfer.   
				Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "ForceTransferMoney");

				// Handle the return values from Money Server.  
				if (resultTable!=null && resultTable.Contains("success"))
				{
                    if (bool.Parse(resultTable["success"].ToString()))
					{
						ret = ETransferMoneyReturn.SUCCESS;
					}
				}
				else 
                {
                    m_log.ErrorFormat("[MONEY] ForceTransferMoney: Cannot force transfer money from [{0}] to [{1}]", sender.ToString(), receiver.ToString());
                }
			}
            else
            {
                m_log.ErrorFormat("[MONEY] GetTransactionInfo: Invalid Money Server URL");
            }

			#endregion

			return ret;
		}


		/// <summary>   
		/// Send the money to avatar. Need to notify money server to update.   
		/// </summary>   
		/// <param name="amount">   
		/// The amount of money.  
		/// </param>   
		/// <returns>   
		/// return true, if successfully.   
		/// </returns>   
		private bool SendMoneyBalance(UUID avatarID, int amount, string secretCode)
		{
			bool ret = false;

            if (!string.IsNullOrEmpty(m_moneyServURL))
            {
                // Fill parameters for money transfer XML-RPC.   
                Hashtable paramTable = new Hashtable();
                paramTable["avatarID"] = avatarID.ToString();
                paramTable["transactionType"] = (int)MoneyTransactionType.ReferBonus;
                paramTable["amount"] = amount;
                paramTable["secretAccessCode"] = secretCode;
                paramTable["description"] = "System balance adjustment";

                // Generate the request for transfer.   
                Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "SendMoneyBalance");

                // Handle the return values from Money Server.  
                if (resultTable != null && resultTable.Contains("success"))
                {
                    if (bool.Parse(resultTable["success"].ToString()))
                    {
                        ret = true;
                    }
                    else
                    {
                        m_log.ErrorFormat("[MONEY] SendMoneyBalance: Failed: {0}", resultTable["message"]);
                    }
                }
                else
                {
                    m_log.ErrorFormat("[MONEY] SendMoneyBalance: Money Server is not responding");
                }
            }
            else
            {
                m_log.ErrorFormat("[MONEY] GetTransactionInfo: Invalid Money Server URL");
            }

			return ret;
		}



		/// <summary>   
		/// Pay the money of charge.
		/// </summary>   
		/// <param name="amount">   
		/// The amount of money.   
		/// </param>   
		/// <returns>   
		/// return ETransferMoneyReturn  
		/// </returns>   
        private ETransferMoneyReturn PayMoneyCharge(UUID sender, int amount, MoneyTransactionType type, ulong regionHandle, string description)
		{
			ETransferMoneyReturn ret = ETransferMoneyReturn.FAILURE;
			IClientAPI senderClient = GetLocateClient(sender);

			// Handle illegal transactions.   
			if (senderClient == null) // receiverClient could be null.
			{
				m_log.InfoFormat("[MONEY] PayMoneyCharge: Client {0} is not found", sender.ToString());
				return ETransferMoneyReturn.INVALID_AGENT;
			}

			if (QueryBalanceFromMoneyServer(senderClient) < amount)
			{
                m_log.InfoFormat("[MONEY] TransferMoney Insufficient funds to cover ${0} charge for client [{1}]", amount, sender.ToString());
				return ETransferMoneyReturn.INSUFF_FUNDS;
			}

			if (!string.IsNullOrEmpty(m_moneyServURL))
			{
				// Fill parameters for money transfer XML-RPC.   
				Hashtable paramTable = new Hashtable();
				paramTable["senderID"] 				= sender.ToString();
				paramTable["senderSessionID"] 		= senderClient.SessionId.ToString();
				paramTable["senderSecureSessionID"] = senderClient.SecureSessionId.ToString();
                paramTable["transactionType"] 		= (int)type;
				paramTable["amount"] 				= amount;
				paramTable["regionHandle"] 			= regionHandle.ToString();
                if (!String.IsNullOrEmpty(description))
				    paramTable["description"] 		= description;

				// Generate the request for transfer.   
				Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "PayMoneyCharge");

				// Handle the return values from Money Server.  
				if (resultTable!=null && resultTable.Contains("success"))
				{
                    if (bool.Parse(resultTable["success"].ToString()))
					{
						ret = ETransferMoneyReturn.SUCCESS;
					}
                    else
                    {
                        if (resultTable.Contains("errorMessage")
                            && resultTable["errorMessage"].ToString() == TIMEOUT_OCCURED)
                        {
                            ret = ETransferMoneyReturn.EXPIRED;
                        }   
                    }
				}
				else 
                {
                    m_log.ErrorFormat("[MONEY] PayMoneyCharge: Failed payment request from [{0}]", sender.ToString());
                }
			}
			else 
            {
                ret = ETransferMoneyReturn.DISABLED;
                m_log.ErrorFormat("[MONEY] GetTransactionInfo: Invalid Money Server URL");
            }

			return ret;
		}



		/// <summary>   
		/// Login to the money server at client login.
		/// </summary>   
		/// <param name="userID">   
		/// Indicate user ID of the new client.   
		/// </param>   
		/// <returns>   
		/// return true, if successfully.   
		/// </returns>   
		private bool LoginMoneyServer(IClientAPI client, out int balance)
		{
			bool ret = false;
			balance = 0;

			#region Send money server the client info for login.

			Scene scene = (Scene)client.Scene;

			if (!string.IsNullOrEmpty(m_moneyServURL))
			{
				// Get the username for the login user.
                string userName = string.Empty;
                if (client.Scene is Scene && scene != null)
				{
					userName = scene.UserManagementModule.GetUserName(client.AgentId);
				}

				// Login the Money Server.   
				Hashtable paramTable = new Hashtable();
				paramTable["openSimServIP"] 		= scene.RegionInfo.ServerURI.Replace(scene.RegionInfo.InternalEndPoint.Port.ToString(), 
																						 scene.RegionInfo.HttpPort.ToString());
				paramTable["userName"] 				= userName;
				paramTable["clientUUID"] 			= client.AgentId.ToString();
				paramTable["clientSessionID"] 		= client.SessionId.ToString();
				paramTable["clientSecureSessionID"] = client.SecureSessionId.ToString();

				// Generate the request for transfer.   
				Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "ClientLogin");

				// Handle the return result 
				if (resultTable!=null && resultTable.Contains("success"))
				{
                    if (bool.Parse(resultTable["success"].ToString()))
					{
						balance = (int)resultTable["clientBalance"];
						m_log.InfoFormat("[MONEY] LoginMoneyServer: Client [{0}] login Money Server {1}", client.AgentId.ToString(), m_moneyServURL);
						ret = true;
					}
				}
				else 
                {
                    m_log.ErrorFormat("[MONEY] LoginMoneyServer: Unable to login Money Server {0} for client [{1}]", m_moneyServURL, client.AgentId.ToString());
                }
			}
			else 
            {
                m_log.ErrorFormat("[MONEY] GetTransactionInfo: Invalid Money Server URL");
            }

			#endregion

			return ret;
		}



		/// <summary>   
		/// Log off from the money server.   
		/// </summary>   
		/// <param name="userID">   
		/// Indicate user ID of the new client.   
		/// </param>   
		/// <returns>   
		/// return true, if successfully.   
		/// </returns>   
		private bool LogoffMoneyServer(IClientAPI client)
		{
			bool ret = false;

			if (!string.IsNullOrEmpty(m_moneyServURL))
			{
				// Log off from the Money Server.   
				Hashtable paramTable = new Hashtable();
				paramTable["clientUUID"] 			= client.AgentId.ToString();
				paramTable["clientSessionID"] 		= client.SessionId.ToString();
				paramTable["clientSecureSessionID"] = client.SecureSessionId.ToString();

				// Generate the request for transfer.   
				Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "ClientLogout");
				// Handle the return result
				if (resultTable!=null && resultTable.Contains("success"))
				{
                    if (bool.Parse(resultTable["success"].ToString()))
					{
						ret = true;
					}
				}
			}

			return ret;
		}



		/// <summary>   
		/// Generic XMLRPC client abstraction   
		/// </summary>   
		/// <param name="ReqParams">Hashtable containing parameters to the method</param>   
		/// <param name="method">Method to invoke</param>   
		/// <returns>Hashtable with success=>bool and other values</returns>   
		private Hashtable genericCurrencyXMLRPCRequest(Hashtable reqParams, string method)
		{
			if (reqParams.Count <= 0 || string.IsNullOrEmpty(method)) 
                return null;

			if (m_checkServerCert)
			{
				if (!m_moneyServURL.StartsWith("https://"))
				{
					m_log.InfoFormat("[MONEY] genericCurrencyXMLRPCRequest: CheckServerCert is true, but protocol is not HTTPS. Please check INI file");

				}
			}
			else
			{
				if (!m_moneyServURL.StartsWith("https://") && !m_moneyServURL.StartsWith("http://"))
				{
					m_log.ErrorFormat("[MONEY] genericCurrencyXMLRPCRequest: Invalid Money Server URL: {0}", m_moneyServURL);
					return null;
				}
			}


			ArrayList arrayParams = new ArrayList();
			arrayParams.Add(reqParams);
			XmlRpcResponse moneyServResp = null;
			try
			{
				NSLXmlRpcRequest moneyModuleReq = new NSLXmlRpcRequest(method, arrayParams);
				moneyServResp = moneyModuleReq.certSend(m_moneyServURL, m_cert, m_checkServerCert, m_RequestTimeout);
			}
            catch (WebException we)
            {
                if (we.Status == WebExceptionStatus.Timeout)
                {
                    m_log.Error("[MONEY] genericCurrencyXMLRPCRequest: A timeout occured while processing the transaction.");
                    Hashtable ErrorHash = new Hashtable();
                    ErrorHash["success"] = false;
                    ErrorHash["errorMessage"] = TIMEOUT_OCCURED;
                    ErrorHash["errorURI"] = "";
                    return ErrorHash;
                }
                else if (we.Status == WebExceptionStatus.ProtocolError)
                {
                    using (HttpWebResponse webResponse = (HttpWebResponse)we.Response)
                        m_log.ErrorFormat("[MONEY] genericCurrencyXMLRPCRequest:  WebException in certSend: [{0}] {1}", webResponse.StatusCode, webResponse.StatusDescription);
                    Hashtable ErrorHash = new Hashtable();
                    ErrorHash["success"] = false;
                    ErrorHash["errorMessage"] = "Unable to manage money at this time.";
                    ErrorHash["errorURI"] = "";
                    return ErrorHash;
                }
            }
			catch (Exception ex)
			{
				m_log.ErrorFormat("[MONEY] genericCurrencyXMLRPCRequest: Unable to connect to Money Server at {0}", m_moneyServURL);
                m_log.ErrorFormat("[MONEY] genericCurrencyXMLRPCRequest: {0}", ex.Message);

				Hashtable ErrorHash = new Hashtable();
				ErrorHash["success"] = false;
				ErrorHash["errorMessage"] = "Unable to manage money at this time.";
				ErrorHash["errorURI"] = "";
				return ErrorHash;
			}
			if (moneyServResp.IsFault)
			{
				Hashtable ErrorHash = new Hashtable();
				ErrorHash["success"] = false;
				ErrorHash["errorMessage"] = "Unable to manage money at this time.";
				ErrorHash["errorURI"] = "";
				return ErrorHash;
			}

			Hashtable moneyRespData = (Hashtable)moneyServResp.Value;
			return moneyRespData;
		}



		private int QueryBalanceFromMoneyServer(IClientAPI client)
		{
            int ret = int.MinValue;

			if (client != null)
			{
				if (!string.IsNullOrEmpty(m_moneyServURL))
				{
					Hashtable paramTable = new Hashtable();
					paramTable["clientUUID"] 			= client.AgentId.ToString();
					paramTable["clientSessionID"] 		= client.SessionId.ToString();
					paramTable["clientSecureSessionID"] = client.SecureSessionId.ToString();

					// Generate the request for transfer.   
					Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "GetBalance");

					// Handle the return result
					if (resultTable!=null && resultTable.Contains("success"))
					{
                        if (bool.Parse(resultTable["success"].ToString()))
						{
							ret = (int)resultTable["clientBalance"];
						}
					}
				}
				else
				{
					if (m_moneyServer.ContainsKey(client.AgentId))
					{
						ret = m_moneyServer[client.AgentId];
					}
				}

				if (ret < 0)
				{
					m_log.ErrorFormat("[MONEY] QueryBalanceFromMoneyServer: Unable to query balance from Money Server {0} for client [{1}]", 
																					m_moneyServURL, client.AgentId.ToString());
				}
			}

			return ret;
		}



		//
		private EventManager.MoneyTransferArgs GetTransactionInfo(IClientAPI client, string transactionID)
		{
			EventManager.MoneyTransferArgs args = null;

			if (!string.IsNullOrEmpty(m_moneyServURL))
			{
				Hashtable paramTable = new Hashtable();
				paramTable["clientUUID"]			= client.AgentId.ToString();
				paramTable["clientSessionID"]		= client.SessionId.ToString();			
				paramTable["clientSecureSessionID"] = client.SecureSessionId.ToString();
				paramTable["transactionID"]			= transactionID;

				// Generate the request for transfer.   
				Hashtable resultTable = genericCurrencyXMLRPCRequest(paramTable, "GetTransaction");

				// Handle the return result
				if (resultTable!=null && resultTable.Contains("success"))
				{
                    if (bool.Parse(resultTable["success"].ToString()))
					{
						int amount  = (int)resultTable["amount"];
						int type	= (int)resultTable["type"];
						string desc = (string)resultTable["description"];
						UUID sender = UUID.Zero;
						UUID recver = UUID.Zero;
						UUID.TryParse((string)resultTable["sender"],   out sender);
						UUID.TryParse((string)resultTable["receiver"], out recver);
						args = new EventManager.MoneyTransferArgs(sender, recver, amount, type, desc);
					}
					else 
					{
						m_log.ErrorFormat("[MONEY] GetTransactionInfo: Request failed: {0}", (string)resultTable["description"]);
					}
				}
				else 
				{
					m_log.ErrorFormat("[MONEY] GetTransactionInfo: Invalid Response");
				}
			}
			else
			{
				m_log.ErrorFormat("[MONEY] GetTransactionInfo: Invalid Money Server URL");
			}

			return args;
		}



		/// Locates a IClientAPI for the client specified   
		/// </summary>   
		/// <param name="AgentID"></param>   
		/// <returns></returns>   
		private IClientAPI GetLocateClient(UUID AgentID)
		{
			IClientAPI client = null;

			lock (m_sceneList)
			{
				if (m_sceneList.Count > 0)
				{
					foreach (Scene _scene in m_sceneList.Values)
					{
						ScenePresence tPresence = _scene.GetScenePresence(AgentID);
						if (tPresence != null && !tPresence.IsChildAgent)
						{
							IClientAPI rclient = tPresence.ControllingClient;
							if (rclient != null)
							{
								client = rclient;
								break;
							}
						}
					}
				}
			}

			return client;
		}



		private Scene GetLocateScene(UUID AgentId)
		{
			Scene scene = null;

			lock (m_sceneList)
			{
				if (m_sceneList.Count > 0)
				{
					foreach (Scene _scene in m_sceneList.Values)
					{
						ScenePresence tPresence = _scene.GetScenePresence(AgentId);
						if (tPresence != null && !tPresence.IsChildAgent)
						{
							scene = _scene;
							break;
						}
					}
				}
			}

			return scene;
		}

        private SceneObjectPart GetLocatePrim(UUID objectID)
        {
            SceneObjectPart sceneObj = null;

            lock (m_sceneList)
            {
                if (m_sceneList.Count>0)
                {
                    foreach (Scene _scene in m_sceneList.Values)
                    {
                        SceneObjectPart part = _scene.GetSceneObjectPart(objectID);
                        if (part!=null)
                        {
                            sceneObj = part;
                            break;
                        }
                    }
                }
            }

            return sceneObj;
        }

		#endregion
	}

}
