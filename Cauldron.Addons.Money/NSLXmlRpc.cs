/* 
 * Copyright (c) Contributors, http://www.nsl.tuis.ac.jp
 * Copyright (c) 2015 Cinder Roxley and Ultimate Gaming Europe, AB
 *
 */

using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using log4net;
using Nwc.XmlRpc;

namespace NSL.Network.XmlRpc 
{
    public class NSLXmlRpcRequest : XmlRpcRequest
    {
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private Encoding _encoding = new ASCIIEncoding();
        private XmlRpcRequestSerializer _serializer = new XmlRpcRequestSerializer();
        private XmlRpcResponseDeserializer _deserializer = new XmlRpcResponseDeserializer();


        public NSLXmlRpcRequest()
        {
            _params = new ArrayList();
        }


        public NSLXmlRpcRequest(String methodName, IList parameters)
        {
            MethodName = methodName;
            _params = parameters;
        }


        public XmlRpcResponse certSend(String url, X509Certificate2 clientCert, bool checkServerCert, Int32 timeout)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            if (request == null)
            {
                throw new XmlRpcException(XmlRpcErrorCodes.TRANSPORT_ERROR, 
                    String.Concat(XmlRpcErrorCodes.TRANSPORT_ERROR_MSG,": Could not create request with ", url));
            }

            request.Method = "POST";
            request.ContentType = "text/xml";
            request.AllowWriteStreamBuffering = true;
            request.Timeout = timeout;
            request.UserAgent = "NSLXmlRpcRequest";

            if (clientCert!=null) 
                request.ClientCertificates.Add(clientCert); // Our client certificate
            if (!checkServerCert) 
                request.Headers.Add("NoVerifyCert", "true");    // Does not validate remote certificate

            using (Stream stream = request.GetRequestStream()) 
            {
                using (XmlTextWriter xml = new XmlTextWriter(stream, _encoding))
                {
                    _serializer.Serialize(xml, this);
                    xml.Flush();
                }
            }

            HttpWebResponse response = null;
            try 
            { 
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException we)
            {
                using (HttpWebResponse webResponse = (HttpWebResponse)we.Response)
                    m_log.ErrorFormat("[MONEY] XmlRpcResponse certSend: GetResponse Error: [{0}] {1}", webResponse.StatusCode, webResponse.StatusDescription);
                if (response != null)
                    ((IDisposable)response).Dispose();
                throw;
            }
            catch (Exception ex) 
            {
                m_log.ErrorFormat("[MONEY] XmlRpcResponse certSend: GetResponse Error: {0}", ex);
                if (response != null)
                    ((IDisposable)response).Dispose();
                throw;
            }
            using (StreamReader input = new StreamReader(response.GetResponseStream()))
            {
                string inputXml = input.ReadToEnd();
                XmlRpcResponse resp = (XmlRpcResponse)_deserializer.Deserialize(inputXml);
                input.Close();
                response.Close();
                return resp;
            }
        }
    }
}
