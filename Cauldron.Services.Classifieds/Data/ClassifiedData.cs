﻿using System;
using System.Collections.Generic;
using OpenMetaverse;

namespace Cauldron.Services.Classifieds.Data
{
    public class ClassifiedData
    {
        public enum ClassifiedFlags : int
        {
            ClassifiedFlagNone = 1 << 0,
            ClassifiedFlagMature = 1 << 1,
            //ClassifiedFlagEnabled	= 1 << 2, // see llclassifiedflags.cpp
            //ClassifiedFlagHasPrice= 1 << 3, // deprecated
            ClassifiedFlagUpdateTime = 1 << 4,
            ClassifiedFlagAutoRenew = 1 << 5,
        }

        public ClassifiedData()
        {
        }

        public ClassifiedData(UUID classifiedId)
        {
            classifieduuid = classifiedId;
            creatoruuid = UUID.Zero;
            creationdate = 0;
            expirationdate = 0;
            category = 0;
            name = string.Empty;
            description = string.Empty;
            parceluuid = UUID.Zero;
            parentestate = 0;
            snapshotuuid = UUID.Zero;
            simname = string.Empty;
            posglobal = "<0,0,0>";
            parcelname = string.Empty;
            classifiedflags = 0;
            priceforlisting = 0;
        }

        public ClassifiedData(Dictionary<string, object> kvp)
        {
            if (kvp.ContainsKey("classifieduuid"))
                UUID.TryParse(kvp["classifieduuid"].ToString(), out classifieduuid);
            if (kvp.ContainsKey("creatoruuid"))
                UUID.TryParse(kvp["creatoruuid"].ToString(), out creatoruuid);
            if (kvp.ContainsKey("creationdate"))
                creationdate = Int32.Parse(kvp["creationdate"].ToString());
            if (kvp.ContainsKey("expirationdate"))
                expirationdate = Int32.Parse(kvp["expirationdate"].ToString());
            if (kvp.ContainsKey("category"))
                category = Int32.Parse(kvp["category"].ToString());
            if (kvp.ContainsKey("name"))
                name = kvp["name"].ToString();
            if (kvp.ContainsKey("description"))
                description = kvp["description"].ToString();
            if (kvp.ContainsKey("parceluuid"))
                UUID.TryParse(kvp["parceluuid"].ToString(), out parceluuid);
            if (kvp.ContainsKey("parentestate"))
                parentestate = Int32.Parse(kvp["parentestate"].ToString());
            if (kvp.ContainsKey("snapshotuuid"))
                UUID.TryParse(kvp["snapshotuuid"].ToString(), out snapshotuuid);
            if (kvp.ContainsKey("simname"))
                simname = kvp["simname"].ToString();
            if (kvp.ContainsKey("posglobal"))
                posglobal = kvp["posglobal"].ToString();
            if (kvp.ContainsKey("parcelname"))
                parcelname = kvp["parcelname"].ToString();
            if (kvp.ContainsKey("classifiedflags"))
                classifiedflags = Byte.Parse(kvp["classifiedflags"].ToString());
            if (kvp.ContainsKey("priceforlisting"))
                priceforlisting = Int32.Parse(kvp["priceforlisting"].ToString());
        }

        public Dictionary<string, object> ToKeyValuePairs()
        {
            Dictionary<string, object> result = new Dictionary<string, object>
            {
                ["classifieduuid"] = classifieduuid.ToString(),
                ["creatoruuid"] = creatoruuid.ToString(),
                ["creationdate"] = creationdate.ToString(),
                ["expirationdate"] = expirationdate.ToString(),
                ["category"] = category.ToString(),
                ["name"] = name,
                ["description"] = description,
                ["parceluuid"] = parceluuid.ToString(),
                ["parentestate"] = parentestate.ToString(),
                ["snapshotuuid"] = snapshotuuid.ToString(),
                ["simname"] = simname,
                ["posglobal"] = posglobal,
                ["parcelname"] = parcelname,
                ["classifiedflags"] = classifiedflags.ToString(),
                ["priceforlisting"] = priceforlisting.ToString()
            };
            return result;
        }

        public UUID classifieduuid;
        public UUID creatoruuid;
        public int creationdate;
        public int expirationdate;
        public int category;
        public string name;
        public string description;
        public UUID parceluuid;
        public int parentestate;
        public UUID snapshotuuid;
        public string simname;
        public string posglobal;
        public string parcelname;
        public int classifiedflags;
        public int priceforlisting;
        public Dictionary<string, string> Data;
    }
}
